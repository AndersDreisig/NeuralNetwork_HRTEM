========================================================================== 
Convolutional Neural Networks for analyzing atomic-resolution HRTEM images
==========================================================================


This is a collection of Python modules, scripts and Jupyter notebooks
for using Convolutional Neural Networks to identify atoms, atomic
columns and possibly other atomic-scale structures in High Resolution
Transmission Electron Microscopy (HRTEM) images.

This code is based on Jacob Madsen's code at
https://github.com/jacobjma/atomic-resolution-tensorflow.  Major parts
of the code are Jacob's, but the neural network itself has been
reimplemented with `Keras <http://keras.io>`_ instead of TensorFlow,
since the latter requires a significant amount of machine learning
expertise (or at least did when Jacob wrote his code).  Keras provides
a high-level interface and sensible defaults for most parameters.

The neural network can be trained on simulated images and subsequently
used on "real" HRTEM images (and image sequences) - this is
particularly useful for analyzing large amounts of images and long sequences.

What can it do?
===============

We have trained networks on Gold nanoparticles and used the network to
track surface diffusion of gold nanoparticles on an oxide support
under the influence of small amounts of gasses.

We have also trained a network on graphene-like sheets, and used the
network to find atoms in defected sheets of graphene, and thus
calculate the strain field in the graphene.

The methods are described in [JAMAD1]_


Microscopy & Microanalysis 2018
===============================

This work will be presented at the `M&M 2018 conference
<https://www.microscopy.org/mandm/2018/>`_

A snapshot of the code at the time of presentation, as well as Jupyter
Notebooks creating many of the figures for the slides are available in
the ``M_and_M_2018`` folder in this project, see
`M_and_M_2018/README.rst <M_and_M_2018/README.rst>`_.

A PDF of my presentation (without the movies and animations) is
available as well: `M_and_M_2018/MandM18_Schiotz.pdf <M_and_M_2018/MandM18_Schiotz.pdf>`_

Installation
============

Installation instructions are in the file `INSTALL <INSTALL.rst>`_

Folders and Files
=================

Folders
-------

M_and_M_2018
  Snapshot of the software as used for the presentation at **Microscopy
  and Microanalysis 2018** in Baltimore.  This folder also contains
  notebooks used for making most of the images and videos in the presentation.

temnn
  Folders containing the software

  temnn/knet
    The Keras implementation of the Neural Network

  temnn/net
    Jacob Madsen's original code.  Contains both the raw TensorFlow
    implementation of the code (which is not used but kept for
    reference as it is the basis for [JAMAD1]_), and the files for
    handling simulated TEM dataset.

    dataset.py
      Defines objects for handling simulated TEM dataset and for
      operations on it.

  temnn/analysis
    Legacy code for data analysis.  Currently not used.

simulation
  Scripts for generating training and test sets.  Python files
  starting with ``make_`` are scripts generating training and test
  sets.  Other Python files contain code used by these scripts.  The
  two Jupyter Notebooks are prototypes of the main Python scripts.

Logo
  Notebook for generating the project's GitLab logo from a figure in
  [JAMAD1]_.

Jupyter Notebooks
-----------------

Jupyter notebooks are used for simple tasks that are most easily done
interactively, and for prototyping operations that are later run as
scripts.  The most important notebooks are

ValidateVisual.ipynb
  Plot the raw output from the CNN together with the peaks found in
  both CNN output and ground truth.  Useful for figuring out what goes
  wrong when precision or recall drops.

Try expt with CNN.ipynb
  Illustrates loading an experimental image and running it through a
  pre-trained network.

Make Movie Intro.ipynb
  Illustrates how to make a movie from an image sequence that has
  already been analyzed with the CNN (analysis of sequences requires a
  GPU and cannot be done on my laptop).  The movie was used for the
  third slide at my M&M 2018 presentation.

Make Movie Draft.ipynb
  Notebook for experimenting with plotting parameters prior to making
  a movie.

Read Microscopy Files.ipynb
  How to read a .DM4 file in Python.

simulation/cluster_simulation.ipynb
  Drafting the generation of cluster training data.

simulation/graphene.ipynb
  Drafting the generation of graphene training data.


Python scripts
--------------

ktrain.py
  Train a neural network.  Microscopy parameters and input folders are
  for Au nanoparticles in the <110> zone axis.

ktrain_both.py
  As ktrain.py, but trains for both the <110> and <100> zone axis.

ktrain_graphene.py
  As ktrain.py, but for graphene.

learningcurve.py
  Calculates precision and recall for the training set and the
  validation set as a function of epoch.

validatedose.py
  Precision and recall as a function of electron dose.

validatescale.py
  Precision and recall as a function of resolution.  Goes widely
  outside the resolution range used for training in order to
  illustrate the importance of that parameter.

analyze_expt_movie.py
  Analyze an image sequence by running each image through the CNN.
  Output is two folders, one with the raw CNN output, the other with
  the positions of the peaks (much smaller amount of data).

evaluatepeaks.py
  Helper file to calculate precision and recall.  Should be moved
  elsewhere.

simulation/make_cluster_training_data_110.py
  Make the data used by ktrain.py to train on Gold nanoparticles in
  the <110> zone axis.

simulation/make_cluster_testing_data_110.py
  Make the corresponding testing data.

simulation/make_cluster_*_100.py
  Similar scripts for the <100> zone axis

simulation/make_graphene_training.py
  Make random graphene-like structures for training.


References
==========

.. [JAMAD1] Jacob Madsen, Pei Liu, Jens Kling, Jakob Birkedal Wagner,
	    Thomas Willum Hansen, Ole Winther and Jakob Schiøtz: *A
	    Deep Learning Approach to Identify Local Structures in
	    Atomic‐Resolution Transmission Electron Microscopy
	    Images*, Adv. Theory Simul. (in press, 2018).  DOI:
	    `10.1002/adts.201800037
	    <https://doi.org/10.1002/adts.201800037>`_.  Preprint
	    available at https://arxiv.org/abs/1802.03008
	    
